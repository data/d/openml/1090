# OpenML dataset: MercuryinBass

https://www.openml.org/d/1090

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Datasets of Data And Story Library, project illustrating use of basic statistic methods, converted to arff format by Hakan Kjellerstrand.
Source: TunedIT: http://tunedit.org/repo/DASL

DASL file http://lib.stat.cmu.edu/DASL/Datafiles/MercuryinBass.html

Mercury Contamination in Bass

Reference:    Lange, Royals, & Connor. (1993). Transactions of the American Fisheries Society .
Authorization:   contact authors
Description:   Largemouth bass were studied in 53 different Florida lakes to examine the factors that influence the level of mercury contamination.  Water samples were collected from the surface of the middle of each lake in August 1990 and then again in March 1991. The pH level, the amount of chlorophyll, calcium, and alkalinity were measured in each sample. The average of the August and March values were used in the analysis. Next, a sample of fish was taken from each lake with sample sizes ranging from 4 to 44 fish. The age of each fish and mercury concentration in the muscle tissue was measured. (Note: Since fish absorb mercury over time, older fish will tend to have higher concentrations). Thus, to make a fair comparison of the fish in different lakes, the investigators used a regression estimate of the expected mercury concentration in a three year old fish as the standardized value for each lake. Finally, in 10 of the 53 lakes, the age of the individual fish could not be determined and the average mercury concentration ofthe sampled fish was used instead of the standardized value.
Number of cases:   53
Variable Names:

ID:   ID number
Lake:   Name of the lake
Alkalinity:   Alkalinity (mg/L as Calcium Carbonate)
pH:   pH
Calcium:   Calcium (mg/l)
Chlorophyll:   Chlorophyll (mg/l)
Avg_Mercury:   Average mercury concentration (parts per million) in the muscle tissue of the fish sampled from that lake
No.samples:   How many fish were sampled from the lake
min:   Minimum mercury concentration amongst the sampled fish
max:   Maximum mercury concentration amongst the sampled fish
3_yr_Standard_mercury :   Regression estimate of the mercury concentration in a 3 year old fish from the lake (or = Avg Mercury when age data was not available)
age_data:   Indicator of the availability of age data on fish sampled

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1090) of an [OpenML dataset](https://www.openml.org/d/1090). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1090/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1090/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1090/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

